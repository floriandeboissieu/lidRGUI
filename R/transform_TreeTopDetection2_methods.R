# ===============================================================================
#
# PROGRAMMERS:
#
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
#' @export
getTreeTopDetection2Methods <- function() {
  # ALL DTM Normalization methods
  list(
    lidaRtReeMaxima = lidaRtReeMaximaMethod()
  )
}


# -----------------------------------------------
# lidaRtReeMaxima METHOD (the only available at the moment)
# -----------------------------------------------
lidaRtReeMaximaMethod = function() {
  list(
    renderUI = lidaRtReeMaximaRenderUI,
    process = lidaRtReeMaximaProcess
  )
}

lidaRtReeMaximaRenderUI <- function(session) {
  fluidRow(
    column(3,
           tooltip(
      "Non-linear filter for holes filling.",
           selectInput(session$ns("nlFilter_input_param"), "Non-linear filter", c("None", "Closing", "Median")))),
    column(3,
           tooltip(
             "Window size in pixels for non-linear filter",
             numericInput(session$ns("nlSize_input_param"), "nlSize", 5, min=0))),
    column(3,
           tooltip(
             "Gaussian smoothing specified as the standard deviation of filter, in map units",
             numericInput(session$ns("sigma_input_param"), "sigma", 0.3, min=0))),
    column(3,
           tooltip(
             "Minimum tree height",
             numericInput(session$ns("hmin_input_param"), "hmin", 5, min=0))),
    column(3,
           tooltip(
             "Minimum distance of a treetop to nearest higher crown",
             numericInput(session$ns("dmin_input_param"), "dmin", 0, min=0))),
    column(3,
           tooltip(
             "Minimum distance of a treetop to nearest higher crown, as proportion of treetop height",
             numericInput(session$ns("dprop_input_param"), "dprop", 0.05, min=0))),
    column(3,
           tooltip(
             "Minimum crown height",
             numericInput(session$ns("crownMinH_input_param"), "crownMinH", 2, min=0))),
    column(3,
           tooltip(
             "Minimum crown height as proportion of tree top height",
             numericInput(session$ns("crownProp_input_param"), "crownProp", 0.3, min=0, max=1)))
  )
}

lidaRtReeMaximaProcess <- function(input) {
  req(input$nlFilter_input_param, input$nlSize_input_param, input$sigma_input_param, input$hmin_input_param, input$dmin_input_param, input$dprop_input_param, input$crownMinH_input_param, input$crownProp_input_param)
  print("Computation of local maxima, segmentation and tree extraction")
  selectedCHM <- dataModel$getData(input$list_input_data)$data
  # tree segmentation
  plots.segments <- lapply(selectedCHM,
                           lidaRtRee::treeSegmentation,
                           nlFilter = input$nlFilter_input_param, nlSize = input$nlSize_input_param, sigma = input$sigma_input_param, dmin = input$dmin_input_param, dprop = input$dprop_input_param, hmin = input$hmin_input_param, crownProp = input$crownProp_input_param, crownMinH = input$crownMinH_input_param)
  # tree extraction
  plots.treetops <- lapply(plots.segments, function(x) {
    treetops <- lidaRtRee::treeExtraction(x$filled.dem, x$local.maxima, x$segments.id)
    names(treetops) <- toupper(names(treetops))
    return(treetops)
    })

  return(plots.treetops)
}

