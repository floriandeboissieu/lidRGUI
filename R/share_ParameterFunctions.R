# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
defaultParameters <- function() {
  param <- list()
  param[["workingDirectory"]] <- system.file("extdata", package = "lidRGUI")
  param[["lasCatalogDirectory"]] <- system.file("extdata", "LAZ", package = "lidRGUI")
  param[["inputDirectory"]] <- system.file("extdata", package = "lidRGUI")
  param[["outputDirectory"]] <- system.file("extdata", package = "lidRGUI")
  param[["cores"]] <- parallel::detectCores()
  param[["projection"]] <- "+init=epsg:4326"
  return(param)
}

parametersPath <- function() {
  file.path(system.file("shiny", "gui", package = "lidRGUI"), "lidRGUI_settings.json")
}

#' @export
readParameters <- function() {
  if (!file.exists(parametersPath())) {
    writeParameters(defaultParameters())
  }
  parameters <- jsonlite::fromJSON(parametersPath())
  default = defaultParameters()

  # parameters has more attributes than default
  if(!all(names(parameters) %in% names(default))){
    parameters = parameters[names(parameters) %in% names(default)]
    writeParameters(parameters)
  }

  # parameters has missing attributes compared to default
  if(!all(names(default) %in% names(parameters))){
    parameters = default
  }
  # check existance of directories
  for(x in grep('Directory', names(default), value = TRUE)){
    if(is.null(parameters[[x]]) || !file.exists(parameters[[x]])){
      parameters[[x]] = default[[x]]
      writeParameters(parameters)
    }
  }

  return(parameters)
}

#' @export
writeParameters <- function(newParameters) {
  json <- jsonlite::toJSON(newParameters, pretty = TRUE)
  write(json, file = parametersPath())
}
