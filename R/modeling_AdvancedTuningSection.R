# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
#  AdvancedTuningSection Module

#' @importFrom magrittr "%>%" "%$%"
#' @importFrom caret preProcess createDataPartition rfeControl lmFuncs rfe predictors varImp

#####################################
# UI SECTION
#####################################
#' @export
AdvancedTuningSectionUI <- function(id) {
  ns <- NS(id)

  box(
    shinydashboard::tabBox(
      id = "navbar-advanced-tuning",
      width = 12,
      tabPanel("Prepocessing", preprocessingTabUI(ns("preprocessing_tab"))),
      tabPanel("Training Parameters", trainingTabUI(ns("training_tab"))),
      tabPanel("Feature Selection",  methodEvaluationModuleUI(ns("method_evaluation")))
    ),
    title = "Advanced tuning",
    width = 12,
    collapsible = T,
    collapsed = T
  )
}

#######################################
# SERVER SECTION
#######################################
#' @export
AdvancedTuningSection <- function(input, output, session, featureSelectionEnable) {

  preprocessingParameters <- callModule(preprocessingTab, "preprocessing_tab")
  trainingParameters <- callModule(trainingTab, "training_tab")
  featureSelectionParameters <- callModule(methodEvaluationModule, "method_evaluation")
  observe({
    shinyjs::toggle(selector = '#navbar-advanced-tuning li a[data-value="Feature Selection"]',
                    condition = featureSelectionEnable())
  })
  return(
    list(
      preprocessingParameters = preprocessingParameters,
      trainingParameters = trainingParameters,
      featureSelectionParameters = featureSelectionParameters
    )
  )
  # observeEvent(dataModel(), {
    # computeModel(dataModel(), X_lab(), Y_lab(), prepocessingParameters)

    # Render Model profil
    # output$selected_variable <- renderText({
    #   paste0("Selected variables: ", paste(predictors(lmProfile),collapse = ", "))
    # })
    #
    # output$model_profil_plot <- renderPlot({
    #   ggplot(lmProfile)
    # })
    #
    # output$preprocessing_information <- renderText({
    #   paste(removezvText, removecorText, normalizationText,
    #         yeojohnsonText, collapse = "\n")
    # })
    #
    # output$variable_density_plot <- renderPlot({
    #   reshape2::melt(X) %>% ggplot(aes(x=value)) + facet_wrap(~ variable, ncol=6, scales = "free_y") + geom_density()
    # })
    #
    # output$variable_importance_plot <- renderPlot({
    #   ggplot(fit_varimp,
    #          aes(x=factor(features,levels=rev(features)), y= importance)) +
    #     xlab("Feature") + ylab("Importance") +
    #     geom_bar(stat = "identity") +
    #     coord_flip()
    # })
    #
    #
    # output$observe_train_plot <- renderPlot({
    #   ggplot(ObsVsPred, aes(x = Predicted, y = Observed))+
    #     facet_grid(dataType ~ .)+
    #     geom_point()+
    #     geom_abline(slope = 1, intercept = 0, linetype = "dashed")+
    #     coord_cartesian(xlim = range(ObsVsPred[,c("Observed","Predicted")]),
    #                     ylim = range(ObsVsPred[,c("Observed","Predicted")]))
    #
    # })

    # showModal(dataModal())

  # })



  dataModal <- function() {
    modalDialog(
      title = "Model result",
      tabsetPanel(
        tabPanel(
          "Model profil",
          textOutput(session$ns("selected_variable")),
          plotOutput(session$ns("model_profil_plot"))
        ),
        tabPanel("Preprocessing information", verbatimTextOutput(session$ns("preprocessing_information"))),
        tabPanel("Variable density", plotOutput(session$ns("variable_density_plot"))),
        tabPanel("Variable importance", plotOutput(session$ns("variable_importance_plot"))),
        tabPanel("Observe / Train", plotOutput(session$ns("observe_train_plot")))

      ),
      footer = NULL,
      easyClose = TRUE
    )
  }
}
