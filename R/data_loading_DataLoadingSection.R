# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# MODULE DataLoadingSection

#####################################
# UI SECTION
#####################################
#' @export
DataLoadingSectionUI <- function(id) {
  ns <- NS(id)

  box(
    width = 12,
    title = "IMPORT DATA", status = "primary", solidHeader = TRUE,
    collapsible = TRUE,
    fluidRow(
      shinyjs::useShinyjs(),
      column(4,
        selectInput(ns("select_type_choice"), "DATA TYPE", c(
          "LAS Catalog" = "las-grid-input-div",
          "Plots" = "csv-plot-input-div",
          "Raster" = "raster-input-div",
          "RData" = "r6-object-input-div"
        ))
      ),
      column(8,
        div(
          id = ns("las-grid-input-div"),
          style = "margin-top: 25px;",
          tooltip(
            "Import LAS directory as LAS catalog",
            LasGridButtonUI(ns("las_grid_input")),
            inline = T
          )
        ),
        div(
          id = ns("csv-plot-input-div"),
          style = "margin-top: 25px;",
          tooltip(
            "Import Plots position (id, x, y) from CSV file",
            CSVPlotsInputUI(ns("csv_plot_input"), "PLOTS", "Load Plots CSV file"),
            inline = T
          )
        ),
        div(
          id = ns("raster-input-div"),
          style = "margin-top: 25px;",
          tooltip(
            "Import Raster",
            RasterInputUI(ns("raster_input"), "RASTER", "Load RASTER"),
            inline = T
          )
        ),
        div(
          id = ns("r6-object-input-div"),
          style = "margin-top: 25px;",
          tooltip(
            "Import RData",
            R6ObjectInputUI(ns("r6_object_input"), "R6 OBJECT", "Load R6 OBJECT"),
            inline = T
          )
        )
      )
    )
  )
}

######################################w#
# SERVER SECTION
#######################################
#' @export
DataLoadingSection <- function(input, output, session) {
  ns <- session$ns
  shinyjs::hide(id = ns("csv-plot-input-div"), asis = TRUE)
  shinyjs::hide(id = ns("raster-input-div"), asis = TRUE)
  shinyjs::hide(id = ns("r6-object-input-div"), asis = TRUE)

  currentButton <- ns("las-grid-input-div")

  observeEvent(input$select_type_choice, {
    shinyjs::hide(id = currentButton, asis = TRUE)
    shinyjs::show(id = ns(input$select_type_choice), asis = TRUE)
    currentButton <<- ns(input$select_type_choice)
  })

  default_param <- reactiveValues(projection = parameters[["projection"]],
                                  dir = parameters[["inputDirectory"]])
  callModule(LasGridButton, "las_grid_input", default_param)
  callModule(CSVPlotsInput, "csv_plot_input", default_param)
  callModule(RasterInput, "raster_input", default_param)
  callModule(R6ObjectInput, "r6_object_input", default_param)
}
