# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# ModelTab Module

#####################################
# UI SECTION
#####################################
#' @export
ModelTabUI <- function(id) {
  ns <- NS(id)

  fluidRow(
    ModelSectionUI(ns("model_section")),
    column(8, textOutput(ns("output_message")), style = "color: green"),
    column(4, div(
           runButtonUI(ns("run_button"), "Compute model"),
           style = "float: right; margin-bottom: 15px; margin-right: 25px")
    ),
    modelDisplayUI(ns("model_display"))
  )
}

#######################################
# SERVER SECTION
#######################################
#' @export
ModelTab <- function(input, output, session) {

  modelParameters <- callModule(ModelSection, "model_section", reactive({input$run_button}))
  callModule(modelDisplay, "model_display")


  observeEvent(modelParameters$dataModel(), {
    runButtonServer(session$ns("run_button"), {
      tryCatch({
        new_name <- computeModel(modelParameters)
        output$output_message <- renderText(paste("Successfull : ", new_name))
      }, error = function(e) {
        print(geterrmessage())
        stop(e)
      })
    })
  })

}
