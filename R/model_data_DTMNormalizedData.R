# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
#----------------------------------
# DTMNormalizedData Class
#----------------------------------
#' @importFrom R6 R6Class

#' @export DTMNormalizedData
DTMNormalizedData <- R6Class(
  "DTMNormalizedData",

  inherit = LasListData,

  # PRIVATE FIELD
  private = list(
    static = new.env()
  ),


  # PUBLIC FIELD
  public = list(

    # CONSTRUCTOR
    initialize = function(data, inputs, path = NA, creation_time = as.character(Sys.time()), version = self$version) {
      super$initialize("dtmnormalized")
      self$key <- super$generateName()
      self$data <- data
      self$metaData <- list(
        origin = "DTM normalization",
        inputs = inputs,
        projection = inputs$laslist_input_data$projection,
        creation_time = creation_time,
        path = path,
        version = version
      )
      self$version = version
    },

    copy = function(path) {
      DTMNormalizedData$new(self$data, self$metaData$inputs, path, self$metaData$creation_time, version = self$version)
    }
  )

)
