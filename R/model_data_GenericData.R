# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
#----------------------------------
# GenericData Class
#----------------------------------
#' @importFrom R6 R6Class

#' @export GenericData
GenericData <- R6Class(
  "GenericData",

  private = list(
    prefix = NULL
  ),

  public = list(
    key = NULL,
    data = NULL,
    metaData = NULL,
    version = paste("lidRGUI", packageVersion("lidRGUI"), sep="_"),

    # CONSTRUCTOR
    initialize = function(prefix) {
      private$prefix = prefix
      self$updateKey()
    },

    generateName = function() {
      paste0(private$prefix, "_", private$static$cpt)
    },

    # Useful to avoid key conflict when user load data form R6Object
    # (which have already a key)
    updateKey = function() {
      if (is.null(private$static$cpt))
        private$static$cpt <- 0
      private$static$cpt <- private$static$cpt + 1
      self$key <- self$generateName()
    }
  )

)
