# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# informationModelSelection Module

#####################################
# UI SECTION
#####################################
#' @export
informationModelSelectionUI <- function(id) {
  ns <- NS(id)

  tagList(
    fluidRow(
      column(12, DT::dataTableOutput(ns("model_choices_table")))
    )
  )
}

#######################################
# SERVER SECTION
#######################################
#' @export
informationModelSelection <- function(input, output, session) {


  # ALL COMPUTED MODEL
  models <- reactive({dataModel$getKeys(ModelData$classname)})

  # FORM ALL COMPUTED MODEL TO PRINT THEM INTO DATATABLE
  modelsDT <- reactiveVal(data.frame(name = vector(), path = vector(),
    "feature selection" = vector(), train = vector()))
  observeEvent(models(), {
    req(models())
    df <- constructOneRowOfModelDT(models()[1])
    if(length(models()) > 1)
      for (i in 2:length(models()))
        df <- rbind(df, constructOneRowOfModelDT(models()[i]))
    modelsDT(df)
  })


  # RENDER DATA TABLE OF MODEL TO COMPARE OR TO DETAIL
  output$model_choices_table <- DT::renderDataTable({
    customDatatable(modelsDT())
  })

  # GET SELECTED MODEL IN DATA TABLE
  selectedModel <- reactive({
    req(input$model_choices_table_rows_selected)
    data.frame(lapply(modelsDT()[input$model_choices_table_rows_selected,],
      as.character), stringsAsFactors=FALSE)
  })


  # ---------
  # FUNCTION
  # --------


  constructOneRowOfModelDT <- function(key) {
    dataModel_ <- dataModel$getData(key)
    name <- key
    path <- dataModel_$metaData$path
    featureSelection <- ifelse(is.null(dataModel_$metaData$inputs$model_inputs$feature_selection),
           NA, dataModel_$metaData$inputs$model_inputs$feature_selection$model)
    train <- ifelse(is.null(dataModel_$metaData$inputs$model_inputs$training),
                    NA, dataModel_$metaData$inputs$model_inputs$training$model)
    type <- ifelse(dataModel_$metaData$inputs$model_inputs$classification, "Classification", "Regression")
    return(data.frame(name = name, path = path, type = type,
                      train = train, featureSelection = featureSelection))
  }

  return(selectedModel)

}
