# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
#----------------------------------
# MetricsData Class
#----------------------------------
#' @importFrom R6 R6Class

#' @export MetricsData
MetricsData <- R6Class(
  "MetricsData",

  inherit = GenericData,

  # PRIVATE FIELD
  private = list(
    static = new.env()
  ),

  # PUBLIC FIELD
  public = list(

    # CONSTRUCTOR
    initialize = function(data, inputs, path = NA, creation_time = as.character(Sys.time()), version = self$version) {
      super$initialize("metrics")
      self$key <- super$generateName()
      self$data <- data
      self$metaData <- list(
        origin = "Metric Computation",
        inputs = inputs,
        projection = inputs$laslist_input_data$projection, # not valid for CHM metrics
        creation_time = creation_time,
        path = path,
        version = version
      )
      self$version = version
    },

    copy = function(path) {
      MetricsData$new(self$data, self$metaData$inputs, path, self$metaData$creation_time, version = self$version)
    },

    export = function(filename, overwrite = FALSE){
      if(any(file.exists(filename)) && !overwrite){
        stop("File already exist! Set option to overwrite.")
      }else{
        data.table::fwrite(self$data, file=filename, sep="\t", na = "NaN")
      }
    },

    exportUI = function(id, default_filename = file.path(parameters$outputDirectory, sprintf("%s.txt", self$key))){
      ns = NS(id)
      fluidRow(
        column(12, "Metrics are exported to tab delimited text file"),
        column(12, ""),
        column(3, save_file_dialog_ui(ns("file"), icon=icon("folder-open",lib = "glyphicon"))),
        column(3, checkboxInput(ns("export_overwrite"), label = "Overwrite", value = FALSE), offset = 6),
        column(12, textInput(ns("export_current_file"), label=NULL, value = default_filename, width="100%")),
        column(8, textOutput(ns("output_message")), style = "color: green"),
        column(4, withBusyIndicatorUI(
          actionButton(ns("export_save_button"), "Export"), ns("export_save_button")
        ), align="right"))
    },

    exportServer = function(input, output, session){
      ns = session$ns
      defaultdir <- reactiveVal(parameters$outputDirectory)
      exportFilename <- callModule(save_file_dialog, "file", defaultdir)

      observeEvent(exportFilename(), {
        req(exportFilename())
        updateTextInput(session, "export_current_file", value = exportFilename())
        if(!is.null(exportFilename())){
          parameters$outputDirectory <<- dirname(exportFilename())
          defaultdir(parameters$outputDirectory)
        }

      })

      # Save PLOTS in model and hide modal
      observeEvent(input$export_save_button, {
        export_current_file = isolate(input$export_current_file)
        if(!is.null(export_current_file)){
          withBusyIndicatorServer(ns("export_save_button"),{
            self$export(export_current_file, input$export_overwrite)
            output$output_message <- renderText(paste("File saved in", export_current_file))
          })
        }
      })
    }
  )

)
