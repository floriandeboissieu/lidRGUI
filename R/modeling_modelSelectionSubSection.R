# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# ModelMetricsSelectionSubSection Module

#####################################
# UI SECTION
#####################################
#' @import shinyWidgets
#' @export
ModelSelectionSubSectionUI <- function(id) {
  ns <- NS(id)

  tagList(fluidRow(
    column(12, materialSwitch(
      ns("classification_switch_input_param"), "Regression-Classification",
      right=TRUE
    )),
    column(12, checkboxInput(
      ns("feature_selection_checkbox_input_param"), "Features selection"
    )),
    column(4, uiOutput(ns('feature_selection_function_ui'))),
    column(12, DT::dataTableOutput(ns(
      "model_choices_table"
    ))),
    column(12, checkboxInput(ns("extend_model_list"), label="Extend model list")),
    AdvancedTuningSectionUI(ns("advanced_tuning_section"))
  ))
}

#######################################
# SERVER SECTION
#######################################
#' @export
ModelSelectionSubSection <- function(input, output, session) {
  # ---------------------
  # FUNCTION
  # ---------------------
  ns <- session$ns

  advancedTuningParameters <- callModule(AdvancedTuningSection, "advanced_tuning_section", reactive(input$feature_selection_checkbox_input_param))


  constructModelInfo <- function() {
    mods <- caret::getModelInfo()

    num_param <-
      unlist(lapply(mods, function(x)
        paste(
          as.character(x$param$parameter),
          sep = "",
          collapse = ", "
        )))
    num_param[num_param == "parameter"] <- "None"
    num_param <-
      data.frame(model = names(num_param), num_param = num_param)

    mod_type <-
      unlist(lapply(mods, function(x)
        paste(
          sort(x$type), sep = "", collapse = ", "
        )))
    mod_type <- data.frame(model = names(mod_type), type = mod_type)

    libs <-
      unlist(lapply(mods, function(x)
        paste(
          x$library, sep = "", collapse = ", "
        )))
    libs <- data.frame(model = names(libs), libraries = libs)

    mod_name <- unlist(lapply(mods, function(x)
      x$label))
    mod_name <- data.frame(model = names(mod_name), name = mod_name)

    model_info <- merge(mod_name, mod_type, all = TRUE)
    model_info <- merge(model_info, libs, all = TRUE)
    model_info <- merge(model_info, num_param, all = TRUE)
    model_info <-
      model_info[, c('model', 'name', 'type', 'libraries', 'num_param')]
    model_info <- model_info[order(model_info$name), ]
    # restriction to regression algorithms for the moment
    # model_info <- model_info[grepl("regression", model_info$type, ignore.case = TRUE),]
    data.table::as.data.table(model_info, check.names=F)
  }

  observe({
    shinyjs::toggle(ns("feature_selection_function_ui"),
                    condition = (input$feature_selection_checkbox_input_param == TRUE),
                    asis = TRUE)
    shinyjs::toggle(
      ns("model_choices_table"),
      condition = (input$feature_selection_function_input_param == "caretFuncs" ||
                     input$feature_selection_checkbox_input_param == FALSE),
      asis = TRUE
    )
    shinyjs::toggle(
      ns("extend_model_list"),
      condition = (input$feature_selection_checkbox_input_param == FALSE),
      asis = TRUE
    )
  })

  observe({
    if(input$classification_switch_input_param)
      output$feature_selection_function_ui <- renderUI(selectInput(
        ns("feature_selection_function_input_param"),
        "",
        choices = c(
          "rfFuncs",
          "nbFuncs",
          "treebagFuncs",
          "caretFuncs"
        )))
    else
      output$feature_selection_function_ui <- renderUI(selectInput(
        ns("feature_selection_function_input_param"),
        "",
        choices = c(
          "lmFuncs",
          "rfFuncs",
          "nbFuncs",
          "treebagFuncs",
          "caretFuncs"
        )))
  })

  all_models <- constructModelInfo()
  model_info <- reactive({

    if(input$classification_switch_input_param)
      model_type = "Classification"
    else
      model_type = "Regression"

    if(input$extend_model_list){
      subset(all_models, grepl(model_type, type, ignore.case = TRUE))
    }else{
      if(input$classification_switch_input_param)
        subset(all_models, grepl(model_type, type, ignore.case = TRUE) &
                 model %in% c("lda", "knn", "nb", "rf", "svmRadial"))
      else
        subset(all_models, grepl(model_type, type, ignore.case = TRUE) &
                 model %in% c("lm", "rf", "svmRadial", "pls"))
    }
  })

  observe({
    output$model_choices_table <- DT::renderDataTable({
      DT::datatable(
        model_info(),
        rownames = FALSE,
        colnames = c(
          "Model",
          "Description",
          "Type",
          "Libraries",
          "Tuning Parameters"
        ),
        selection = "single",
        options = list(
          deferRender = TRUE,
          scrollY = 200,
          scrollX = TRUE,
          scroller = TRUE
        )
      )
    })
  })


  selectedModel <- reactive({
    req(input$model_choices_table_rows_selected)
    req(model_info)
    model_info()[input$model_choices_table_rows_selected, ]
  })


  inputs <- reactiveVal()
  observe({
    tmp <- extractInputs(input)
    tmp$train_model_input_param <- selectedModel()
    inputs(tmp)
  })
  # inputs <- reactive({
  #   cat(yaml::as.yaml(extractInputs(input)))
  #   df <- extractInputs(input)
  #   df

  # })

  return(
    reactive({
      list(
        classification = input$classification_switch_input_param,
        featureSelection = input$feature_selection_checkbox_input_param,
        modelType = input$feature_selection_function_input_param,
        selectedModel = selectedModel,
        advancedTuning = advancedTuningParameters,
        inputs = inputs
      )
    })
  )
}
