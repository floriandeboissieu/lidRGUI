# ===============================================================================
#
# PROGRAMMERS:
#
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# ----------------
# FUNCTION SECTION
# ----------------

#' Create leaflet map object with openStreetMap as background map
#' @export
createLeafletMap <- function(provider = leaflet::providers$OpenStreetMap) {
  leaflet() %>%
    addProviderTiles(provider) %>%
    addTiles() %>% addScaleBar() %>% addResetMapButton()
}

#' Add plots as circle shape on the leaflet map
#' @export
addPlotsShape <- function(map, plots, x_size = 30, y_size = NULL, group = "Plots") {
  if(is.null(y_size) || is.na(y_size)){
    plots_spWGS <- sp::spTransform(plots, sp::CRS("+init=epsg:4326"))
    map %>% addCircles(data = plots_spWGS, label = row.names(plots),
                       group = group, radius = x_size/2)
  }else{# rectangles
    plots.coords=sp::coordinates(plots) %>% as.data.frame()
    plots = Reduce(sp::rbind.SpatialPolygons,lapply(1:nrow(plots.coords),function(i){
      x=plots.coords[i,]
      lasext <- raster::extent(x$X-x_size/2, x$X+x_size/2,
                               x$Y-y_size/2, x$Y+y_size/2)
      e=as(lasext,'SpatialPolygons')
      e@polygons[[1]]@ID=row.names(x)
      sp::proj4string(e) <- sp::proj4string(plots)
      return(e)}))
    plots_spWGS <- sp::spTransform(plots, sp::CRS("+init=epsg:4326"))
    map %>% addPolygons(smoothFactor = 0.5,
                        data = plots_spWGS, group = group)
  }
}

#' Add plots as circle shape on the leaflet map
#' @export
addTreeTopsShape <- function(map, treetops, proj4string, radius = 1, group = "TreeTops") {
  treetops <- data.table::rbindlist(treetops)
  treetops <- sp::SpatialPointsDataFrame(
    coords = treetops[, c("X", "Y"), with=FALSE],
    data = treetops[, !(names(treetops) %in%
                          c("X", "Y")), with=FALSE],
    proj4string = proj4string
  )
  treetops_spWGS <- sp::spTransform(treetops, sp::CRS("+init=epsg:4326"))
  map %>% addCircles(data = treetops_spWGS, label = treetops_spWGS$Z,
                     group = group, color = "red", radius = radius)
}



#' Add catalog as polygon shape on the leaflet map
#' @export
addCatalogShape <- function(map, lascat, projection, group = "LAS Catalog") {
  lascat_sp <- convertLasCatToSpatialPolygon(lascat, projection)
  # leaflet needs polygons defined in WGS84 coordinate system (EPSG:4326)
  lascat_spWGS <- sp::spTransform(lascat_sp, sp::CRS("+init=epsg:4326"))
  map %>% addPolygons(color = "#555555", weight = 1, smoothFactor = 0.5,
                      data = lascat_spWGS, group = group)
}

#' Add raster on leaflet map
#' @export
addRaster <- function(map, raster, group = "Raster", ...){

  prfort <- raster::projectRaster(
    raster::sampleRegular(raster, size = 2e5, asRaster = TRUE, useGDAL = TRUE),
    crs = sp::CRS("+init=epsg:4326"))
  pal <- colorNumeric("YlOrBr", raster::values(prfort),
                      na.color = "transparent", reverse = F)
  map <- map %>% addRasterImage(prfort, color=pal, group = group, ...)
  map <- map %>% addLegend(pal = pal, values = raster::values(prfort), title = "Alt. (m)")
}

#' Add CHM on leaflet map
#' @export
addCHM <- function(map, chms, group = "CHM", ...){

  chmmin=min(sapply(chms, function(x){x@data@min}))
  chmmax=max(sapply(chms, function(x){x@data@max}))

  pal <- colorNumeric("YlOrBr", c(chmmin, chmmax),
                      na.color = "transparent", reverse = F)

  for(chm in chms){
    # prfort <- raster::projectRaster(
    #   chm,
    #   crs = sp::CRS("+init=epsg:4326"))
    map <- map %>% addRasterImage(chm, color=pal, group = group, ...)
  }
  map
}


#' Convert las catalog to spatialPolygon
#' @export
convertLasCatToSpatialPolygon  <- function(lascat, lasproj) {
  if(packageVersion("lidR")>='1.3.0'){ # LAScatalog class changed format
    lascat_sp <- lidR::as.spatial(lascat)
  }else{
    lascat_sp <- Reduce(sp::rbind.SpatialPolygons,plyr::alply(lascat,1,function(x){
      lasext <- raster::extent(x$Min.X, x$Max.X, x$Min.Y, x$Max.Y)
      e=as(lasext,'SpatialPolygons')
      e@polygons[[1]]@ID=basename(x$filename)
      return(e)}))
  }
  # define projection
  sp::proj4string(lascat_sp) <- lasproj
  return(lascat_sp)
}
