# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# MODULE TransformTab

#####################################
# UI SECTION
#####################################
#' @export
TransformTabUI <- function(id) {
  ns <- NS(id)

  tagList(
    fluidRow(extract_plots_ui(ns("extract_plots"))),
    fluidRow(DTMNormalisationSectionUI(ns("dtm_normalisation_section"))),
    fluidRow(CHMSectionUI(ns("chm_section"))),
    fluidRow(TreeTopDetectionSectionUI(ns("tree_top_detection_section"))),
    fluidRow(TreeSegmentationSectionUI(ns("tree_segmentation_section")))
  )

}

#######################################
# SERVER SECTION
#######################################
#' @export
TransformTab <- function(input, output, session) {
  callModule(extract_plots, "extract_plots")
  callModule(DTMNormalisationSection, "dtm_normalisation_section")
  callModule(CHMSection, "chm_section")
  callModule(TreeTopDetectionSection, "tree_top_detection_section")
  callModule(TreeSegmentationSection, "tree_segmentation_section")
}
