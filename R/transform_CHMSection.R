# ===============================================================================
#
# PROGRAMMERS:
#
# Florian de Boissieu
# David Lang
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# MODULE TransformTab

#####################################
# UI SECTION
#####################################
#' @export
CHMSectionUI <- function(id) {
  ns <- NS(id)

  box(
    width = 12,
    title = p("CANOPY HEIGHT MODEL",
              actionButton(ns("info"), "", icon = icon("question", lib="font-awesome"),
                           class = "btn-xs btn-help", title = "Details on method")),
    status = "primary", solidHeader = TRUE,
    collapsible = TRUE,
    fluidRow(
      column(4, uiOutput(ns("laslist_input_data_div"))),
      column(4, uiOutput(ns("res_input_div"))),
      column(4, uiOutput(ns("subcircle_input_div")))
    ),
    fluidRow(
      column(8, textOutput(ns("output_message")), style = "color: green"),
      column(4,
             div(
               runButtonUI(ns("run_button"), "Run Canopy Height Model"),
               style = "float: right"
             )

      )
    )
  )
}

#######################################
# SERVER SECTION
#######################################
#' @export
CHMSection <- function(input, output, session) {

  observeEvent(input$info,{
    browseHelpURL("grid_canopy", "lidR")
  })

  # dynamic rendering of LAS PLOTS selectInput
  output$laslist_input_data_div <- renderUI({
    tooltip(
      "Data of class LasList.",
      selectInput(session$ns("laslist_input_data"),
                  "LAS LIST", dataModel$getKeys(LasListData$classname)))
  })

  # dynamic rendering of parameters
  output$res_input_div <- renderUI({
    numericInput(session$ns("res_input_param"), "Resolution", 0.5, min = 0)
  })

  output$subcircle_input_div <- renderUI({
    numericInput(session$ns("subcircle_input_param"), "Subcircle", 0.1, min = 0)
  })

  # Event trigger of run button click
  observeEvent(input$run_button, {
    req(input$laslist_input_data)
    runButtonServer(session$ns("run_button"), {
      tryCatch({
        plots.chm <- CHM()
        new_name <- saveCHMData(plots.chm)
        output$output_message <- renderText(paste("Successfull Canopy Height Model : ", new_name))
      }, error = function(e) {stop(e)})
    })
  })

  # ----------------
  # function section
  # ----------------

  saveCHMData <- function(plots.chm) {
    newEntry <- CHMData$new(plots.chm, extractInputs(input))
    newEntry %>% dataModel$addData()
    return(newEntry$key)
  }

  CHM <- function() {
    selectedLas <- dataModel$getData(input$laslist_input_data)$data
    res = input$res_input_param
    subcircle = input$subcircle_input_param
    cl = parallel::makeCluster(parameters$cores, outfile = "")
    plots.chm <- parallel::parLapply(cl, selectedLas, rasterCHM, res = res, subcircle = subcircle)
    parallel::stopCluster(cl)
    return(plots.chm)
  }

}

#' Canopy Height Model
#'
#' Same as \code{\link[lidR]{grid_canopy}} but returns a raster.
#' @return raster.
#' @export
rasterCHM <- function(x, res, subcircle){
  r <- raster::as.raster(lidR::grid_canopy(x, res = res, subcircle = subcircle))
  if(packageVersion("lidR")>="1.3.0") # has slot crs
    raster::projection(r) <- x@crs
  return(r)
}



