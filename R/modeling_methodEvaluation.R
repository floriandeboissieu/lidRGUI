# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================

#' @export
getEvaluationMethods <- function() {
  list(
    repeatedcv = getRepeatedcvMethod(),
    LOOCV = getLOOCVMethod(),
    None = getNoneMethod()
  )
}

#' @export
getDefaultEvaluationMethodParameters <- function() {
  list(
    name = "repeatedcv",
    parameters = list(
      number = reactive(5),
      repeats = reactive(5)
    )
  )
}

# -------------------------------------
# GENERIC METHOD FOR CROSSVALIDATION
# -------------------------------------
getCrossValidationUI <- function(session)  {
  tagList(
    fluidRow(
      column(6, numericInput(session$ns("folds_cv_input_param"), "Folds", 5, min = 1, step = 1)),
      column(6, numericInput(session$ns("repeats_cv_input_param"), "Repeats", 5, min = 1, step = 1))
    )
  )
}

# -----------------
# NONE METHOD
# -----------------
getNoneMethod <- function() {
  list(
    renderUI = function(session) {},
    process = function(input) {
      list(name = "none", parameters = list())
    }
  )
}

# -----------------
# REPEATEDCV METHOD
# -----------------
getRepeatedcvMethod <- function() {
  list(
    renderUI = getCrossValidationUI,
    process = getReapeatedcvProcess
  )
}

getReapeatedcvProcess <- function(input)  {
  list(
    name = "repeatedcv",
    parameters = list(
      number = reactive(input$folds_cv_input_param),
      repeats = reactive(input$repeats_cv_input_param)
    )
  )
}

# -----------------
# LOOCV METHOD
# -----------------
getLOOCVMethod <- function() {
  list(
    renderUI = getLOOCVUI,
    process = getLOOCVProcess
  )
}

getLOOCVProcess <- function(input)  {
  list(
    name = "LOOCV",
    parameters = list(
    )
  )
}

getLOOCVUI <- function(session) {
  fluidRow()
}

