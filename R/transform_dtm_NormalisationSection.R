# ===============================================================================
#
# PROGRAMMERS:
#
# David Lang
# Florian de Boissieu
#
# CONTACT: florian.deboissieu@irstea.fr
#
# This file is part of lidRGUI R package, developped by IRSTEA.
#
# lidRGUI is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>
# ===============================================================================
# MODULE TransformTab

#####################################
# UI SECTION
#####################################
#' @export
DTMNormalisationSectionUI <- function(id) {
  ns <- NS(id)

  box(
    width = 12,
    title = p("DTM NORMALISATION",
              actionButton(ns("info"), "", icon = icon("question", lib="font-awesome"),
                           class = "btn-xs btn-help", title = "Details on method")),
    status = "primary", solidHeader = TRUE,
    collapsible = TRUE,
    fluidRow(
      column(4, uiOutput(ns("laslist_input_data_div"))),
      column(4, uiOutput(ns("method_input_param_div"))),
      column(4, uiOutput(ns("methods_parameters_input_div")))
    ),
    fluidRow(
      column(8, textOutput(ns("output_message")), style = "color: green"),
      column(4,
        div(
          runButtonUI(ns("run_button"),"Normalize las data to DTM"),
          style = "float: right"
        )
      )
    )
  )
}

#######################################
# SERVER SECTION
#######################################
#' @export
DTMNormalisationSection <- function(input, output, session) {

  observeEvent(input$info,{
    browseHelpURL("lasnormalize", "lidR")
  })

  DTMMethods <- getDTMNormalizationMethods()

  # dynamic rendering of LAS PLOTS selectInput
  output$laslist_input_data_div <- renderUI({
    tooltip(
      "Data of class LasList, usually extracted plots before other processing.",
      selectInput(session$ns("laslist_input_data"),
                  "LAS LIST", dataModel$getKeys(LasListData$classname)))
  })

  # dynamic rendering of normalization parameters
  output$methods_parameters_input_div <- renderUI({
    req(input$method_input_param)
    DTMMethods[[input$method_input_param]]$renderUI(session)
  })

  # dynamic rendering of Method selectInput
  output$method_input_param_div <- renderUI({
    selectInput(session$ns("method_input_param"), "METHOD", names(DTMMethods))
  })

  # Event trigger of run button click
  observeEvent(input$run_button, {
    req(input$laslist_input_data)
    runButtonServer(session$ns("run_button"), {
      tryCatch({
        plots.lasn <- normalizeToDTM()
        new_name <- saveNormalizedData(plots.lasn)
        output$output_message <- renderText(paste("Successfull normalization : ", new_name))
      }, error = function(e) {stop(e)})
    })
  })

  # ----------------
  # function section
  # ----------------

  saveNormalizedData <- function(plots.lasn) {
    newEntry <- DTMNormalizedData$new(plots.lasn, extractInputs(input), NA)
    newEntry %>% dataModel$addData()
    return(newEntry$key)
  }

  normalizeToDTM <- function() {
    DTMMethods[[input$method_input_param]]$process(input)
  }

}



