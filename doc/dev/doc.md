# LidRGUI development documentation

This document gives the principal problems encountered and some advice to continue the project.  
## Demo

demo was made with OBS (fixing the exact window size in settings), cut with pitivi
then shorten with ffmpeg command line (or with pitivi) and converted to gif with [a web converter](https://www.onlineconverter.com/video-to-gif)
``` 
ffmpeg -i 2018-04-19\ 09-40-23.mp4 -strict -2 -ss 00:00:02 2018-04-19\ 09-40-23.-2s.mp4
```



## 1. Project structure <a name="project-structure"></a>

R package doesn't allow to create subdirectories in "R/" directory. That's why all files are prefixed by their supposed paths to gain time while searching a file. For example with supposed structure like this :

### 1.1 Files's prefixs

```
R
├── button
  ├── button_CSVPlotsInput.R
  ├── button_directoryInputModule.R
  ├── button_fileInput.R
  ├── button_LasGridButton.R
  ├── button_RasterInput.R
├── data
  ├── dataTab.R
  ├── listing
  	├── DataListingSection.R
  ├── loading
  	├── DataLoadingSection.R
  ├── map
  	├── conditionalPlot.R
  	├── LeafletMap.R
  	├── MapSection.R
  ├── tableExplorer
  	├── TableExplorerSection.R
```
You obtain these prefixed files :

```

├── button_CSVPlotsInput.R
├── button_directoryInputModule.R
├── button_fileInput.R
├── button_LasGridButton.R
├── button_RasterInput.R
├── data_dataTab.R
├── data_listing_DataListingSection.R
├── data_loading_DataLoadingSection.R
├── data_map_conditionalPlot.R
├── data_map_LeafletMap.R
├── data_map_MapSection.R
├── data_tableExplorer_TableExplorerSection.R

```

### 1.2 Module organisation
![]("~/Téléchargements/diagramme de sequence.jpg")

### 1.3 conventions
Scalar inputs have suffix identifier `_input_param` and data inputs have suffix identifier `_input_data`. This way the metadata history is added automatically (and recursively for data) to newly created data.

## 2. Package and tools used

### 2.1 Shiny Modules

Shiny modules are used to separate project in multiple logic section. Furthermore, this module can be reused because of namespace utilization. If you want to know more about shiny module you can look (video is really good):

- [rstudio text tutorial](https://shiny.rstudio.com/articles/modules.html)
- [rstudio video tutorial](https://www.rstudio.com/resources/webinars/understanding-shiny-modules/)

### 2.2 ShinyDashboard package

ShinyDashboard package is used in lidRGUI. It make easy to create a dashboard application.  You can find all the documentation about this package [here](https://rstudio.github.io/shinydashboard/).

### 2.3 DT::datatable

DT package is used to render datatable using JavaScript library DataTables. You can find the documentation [here](https://rstudio.github.io/DT/).

### 2.4 ShinyJs

Shinyjs package is used to hide / show, disable / enable ui section. You can find more information about ShinyJs [here](https://github.com/daattali/shinyjs).

#### tips
Frequently when you want to hide a section you want to show it again with a condition. Then prefere use this :
```
shinyjs::toggle(mysection, condition = condition)
```
than :
```
if (condition == T)
	shinyjs::show(mysection)
else
	shinyjs::hide(mysection)
```


## 3. Problems encountered <a name="problem-encountered"></a>

### 3.1 Store data models in global env <a name="data-models-storing"></a>
All data loaded and calculated are saved in memory to be used later. Every data are stored in their corresponding model (MVC inspiration) to separate data storing logic. But models must be reachable from whole app therefore they must be global.

We didn't find solution to create global package variables so we use global environment to store models. To not contaminate users environment, model variables are clean at the end of lidRGUI.

Model are created and deleted like this in gui.R file :

```
# Create all models globals variables
  catalogModel <<- CatalogModel$new()
  csvPlotsModel <<- CSVPlotsModel$new()
  rasterModel <<- RasterModel$new()
  lasPlotsModel <<- LasPlotsModel$new()
  transformationModel <<- TransformationsModel$new()
  metricsModel <<- MetricsModel$new()


  # Delete all models globals variables
  # when app is closed
  on.exit({
    toClean <- c("catalogModel", "csvPlotsModel", "rasterModel", "lasPlotsModel", "transformationModel",
                  "metricsModel"
                 )
    rm(list = toClean, pos = ".GlobalEnv")
    gc()
  })

```

Models are represented by R6 Class that's why we call gc() (garbage collection) to be sure that's R6 Class env are clean too.


### 3.2 R6 Class field initialization
R6 Class field are initialize like this :

```
public = list(
      catalogs = NULL,
      initialize = function() {
        self$catalogs <- reactiveValues()
      },
```

and not like this :
```
public = list(
      catalogs = reactiveValues(),
```
because in the second way "catalogs" field is not reset when you relaunch lidarGUI. You can know more about this [here](https://stackoverflow.com/questions/45081591/r-r6class-global-obj-in-shinyapp/45122305#45122305)



### 3.3 File and directory

#### Input

To allow user to choose files we tested several options:
- base::file.choose : the dialog box does not appear on top of GUI, thus cannot be seen directly by user.
- shiny::fileInput : this is the client browser, i.e. file is uploaded to server via temporary copy. Thus, it is not adapted to lidar data
- shinyFiles::shinyFileChoose() : this a client file browser o the sever file system, so it is adapted to process lidar data present on seever. However, we encountered some lagging for big file systems. Maybe this bug has been fixed in further versions. Details on use are given below.

As all these options were not filling the wanted functionality, we used command line execution depending OS, see `R/custom_chooseFile.R`

#### Output

For output file chooser (i.e. save or export) `shiny::downloadHandler` was used at first. However:
- this function does not work in RStudio viewer
- as for shiny::fileInput, the data is downloaded to the file, thus not adapted for weighting data.

Therefore system command line was used as well for the moment.

#### shinyFiles

used [shinyFiles](https://github.com/thomasp85/shinyFiles) package. To support Windows and Linux we intialize shinyFiles paths in "initializeDisks" function in share_ShareFunction.R.

Warning : the lastest version of shinyFile on CRAN repositories do not allow to use shinyFile within module. You must use the latest version on github. We open an [issue](https://github.com/thomasp85/shinyFiles/issues/60) in their github to inform them of the problem. Maybe this is this fix has been updated on CRAN since then.

### 3.4 Map rendering

We have two way of rendering map with raster and without. When you render map with raster if user have selected a las catalog you have to render it in first. Because "catalog.plot" has no option "add" which allow to draw in the same plot.


### 3.5 Use server variable in conditionalPanel condition

Conditionals panels have javascript condition to fulfill to render panel. If you want to use server variable, you can use this tips :

```
# UI Section
 conditionalPanel(
 	condition = sprintf("output['%s'] != '', ns("boolean_var")), 	        	"panel content"
 )

# SERVER Section
boolean_var <- reactive({ logical condition })
output$boolean_var <- reactive({ boolean_var() })
# add boolean_var to output vars
outputOptions(output, "boolean_var", suspendWhenHidden = FALSE)

 ```


### 3.6 Parameters persistence

Parameters are saved in gui.R because you can't write file from server side. So today parameters are saved when user closes lidRGUI. Parameters are save like that :

```
  on.exit({
    writeParameters(parameters)
  })
```
So parameters are saved where the server is launch. Then it follows that lidRGUI can't be deploy on web server.
If you want to fix that you must develop a javascript way to save file on client side.


## 4. Warning

### Loading R6 object
R6 object are stored / loaded with "save" and "load" function from {base}.
So when you update R6 Data Class and use new methods / field users can't use anymore data saved from older version.


## 5. Interesting lecture


You can find a lot of tips for shiny apps [here](http://deanattali.com/blog/advanced-shiny-tips/). For example we use and mofify their code of their [Busy..." / "Done!" / "Error" feedback after pressing a button](https://github.com/daattali/advanced-shiny/tree/master/busy-indicator) tips.




## 5. Instruction


### 4.1 Metrics List

All metrics used in lidRGUI are store in a list :

```
	 metrics <- list(
    stdmetrics = substitute(
      lidR::stdmetrics(X,Y,Z,Intensity, ScanAngle, ReturnNumber, 			Classification, pulseID, dz = 1)
    ),
    stdmetrics_ctrl = substitute(
      stdmetrics_ctrl(X, Y, Z, ScanAngle)
    )
  )
```

Metrics are display into DT::datatable with two columns : name and package. To get the package pryr::where function is used but package must be loaded. That's why you must import package like this :

	#' @import lidR
In this example stdmetrics and stdmetrics_ctrl belong to lidR package.
