// Taken from https://github.com/AFriendlyRobot/shiny_remove_tabindex_example
// Supposed to resolve https://github.com/thomasp85/shinyFiles/issues/101
// But not working

window.onload = function(event) {
  // Remove modal 'tabindex' listener if the modal is already open on page load
  document.getElementById('shiny-modal').removeAttribute('tabindex');

  // Add event listener for whenever the modal is opened
  document.getElementById('shiny-modal').addEventListener('show', function(event) {
    document.getElementById('shiny-modal').removeAttribute('tabindex');
  });
};
