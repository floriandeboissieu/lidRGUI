function sleep(ms) {
	return new Promise(resolve => setTimeout(resolve, ms));
}

async function clickOnAllTabs() {
	var advancedTunningTabBox = document.getElementById("model_tab-advanced_tuning_section-navbar-advanced-tuning");
	var tabs = advancedTunningTabBox.children;

	for (var i = 1; i < tabs.length - 1; i++) {
		tabs[i].children[0].click();
		await sleep(125);
	}
	tabs[0].children[0].click();			
} 


window.addEventListener("load", function() {
	var modelTabLink = document.querySelectorAll("a[href='#shiny-tab-modelisation']")[0];
	modelTabLink.addEventListener("click", clickOnAllTabs);
});