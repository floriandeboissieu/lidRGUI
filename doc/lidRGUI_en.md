---
title: "Description of lidRGUI"
author: "Florian de Boissieu"
date: "`r Sys.Date()`"
output: 
  html_vignette: default
vignette: >
  %\VignetteIndexEntry{Description of lidRGUI}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---

# Basic commands of lidRGUI
The interface is launched from command line:
```
library(lidRGUI)
lidRGUI()
```
By default, the GUI will be launched in Rstudio's e Viewer. As long as the program is open the current Rstudio session is no longer accessible. To stop the GUI, either press the Stop button on the Viewer, or press the `Esc` key twice.

In order to keep the Rstudio session running while using the GUI, run lidRGUI in background mode (it will appear within the default browser):
```
lidRGUI(bg = TRUE)
```

The interface is composed of 5 main sections available in the sidebar menu:

- Data: data manager (import / export / multiformat data backup) and cartography.
- Transform: extraction and transformation of data
- Metrics: calculating metrics on transformed data
- Modeling: model development (limited to regression models at the moemnt)
- Settings: general parameters of the interface, typically the number of cores for the calculation.

Generally:

  - **rapid information**: hover the tab, the parameter or th button, a message should appear.
  - **help or details**: click on button! [binfo](figures/binfo.png).
  - **run computation**: `RUN` button under each module starts the execution. A green message indicates that everything went well, a red message indicates an error during the process and the result could not be produced.


<img src="figures/GUI.png" width="100%"/>


## Data
Data section has 2 collapsible boxes:

- Import Data to import new data
- Data for the list of imported data or products, and show these on a map.

### Import Data
The interface allows to load 4 types of data:

- __LAS Catalog__: a directory containing LAS/LAZ tiles.
- __Plots__: a file of type csv (or tab/space separated text file) including the coordinates, the dimensions and the identifiers of the plots, and optionnaly other attributes that would be used for modeling.
- __Raster__: a raster file, typically digital terrain model.
- __RData__: a file in .RData (or .rda) format saved from lidRGUI thanks to button ![](figures/bsave.png).

### Data
This section contains the table of imported or created objects. This table includes the following columns:

- __Actions__ which are respectively:
    - history: ![](figures/bhistory.png),
    - export to a file: ![](figures/bexport.png).
- __Name__: the name assigned by lidRGUI,
- __Origin__: the processing/action that created the object,
- __Method__: the method used when several are available (see section Transform),
- __Path__: file path for imported object,
- __Class__: object class.

The 3 buttons above the table are applying to the selected objects:

- save ![](figures/bsave.png): save the objects in a .RData/.rda file. This file will be reloadable via the import of .RData. It can also be loaded into an R session with the `load (" filename_name.rda ")` command, so the data is in the `data` variable. It is therefore advisable to use this button as often as possible.
- remove ![](figures/bremove.png): remove the objects from the list.
- map ![](figures/bmap.png): display the objects in an interactive map. Not all files are viewable on a map. At the moment, it is possible to display LASCatalog, Plots, Raster, CHM, TreeTops class objects. Point cloud objects (ExtractedLasPlots, DTMNormalized, ...) as well as metrics, are not viewable by this means. If requested, an interactive map is appears under the table.

## Transform
Transform section is composed of a series of processing tools applyable to point clouds and derivated products. The tools are presented in usual order of use for a standard extraction of features/metrics from the point cloud:

- Plot Extraction: extract the points inside the plots. **Importantly** this module has a parameter `buffer` that defines a buffer zone around each plot to avoid adge effects. All following processings are applying to plot+buffer point cloud. The buffer is removed at metric computation (see ).
- DTM Normalization: Normalize the point cloud to the digital terrain model, i.e. Z no longer represents altitude but height relative to the ground.
- Canopy Hieght Model: calculation of the canopy height model, i.e. one raster per plot.
- Tree Top Detection: Detection of tree tops on the canopy pattern. The method `treeExtraction (lidaRtRee)` also allows to estimate the surface and volume of the crowns.
- Tree Segmentation: Assign each point of the point cloud to a tree.

Default values are usually the one defined in methods or having worked best in previous studies.


<img src="figures/TransformTab.png" width="100%"/>


## Metrics {#metrics-section}
Metrics section is for calculating metrics. For the moment only two boxes are available:

- Point Cloud Metrics: calculation of statistics on the point cloud itself, i.e. on height, intensity, density of echoes, etc.
- Tree Tops Metrics: calculation of statistics on the tree heights, the surface and volume of crowns. _These metrics are computable only when TreeTops have been extracted with the `treeExtraction (lidaRtRee)` method_.

Each box includes as input the object on which to calculate the metrics. Below are a double table with:

- on the left the available metric sets,
- on the right, the metrics selected for the calculation.

Unlike the Transform section, metrics are computed on the plot without buffer zone. It is the reason why plots objects may be needed as input data.


<img src="figures/MetricsTab.png" width="100%"/>


## Modeling
Modeling section includes two boxes described below:

- Model: definition and configuration of the model.
- Results: display the results of the model.

### Model
The Model sub-section allows to define a model of type $y = f (X)$. For now only regression models are possible. The definition of the model is done through 3 tabs:

- Input: the definition of the inputs, i.e. $X$. As for metrics, the double table allows to select the metrics of several sets of metrics (see illustration below).
- Output: the definition of the output, i.e. $y$.
- Model: the type of models and the training parameters (Advanced tuning). A shortlist of the most used algorithms is presented in the table. The checkbox `Extend model list` extends the list to more than 200 algorithms (cf [https://topepo.github.io/caret/available-models.html](https://topepo.github.io/ caret / available-models.html)). Only the packages corresponding to the shortlist of algorithms are included in the dependencies, the other packages must be installed manually. The checkbox `Feature Selection` allows a feature selection of the type" Recursive Feature Elimination ". The Advanced tuning box allows to adjust several pre-processing parameters (center, reduce, ...) as well as the evaluation parameters of the model (cross-validation method and parameters).


<img src="figures/Modeling_InputTab.png" width="100%"/>


### Results
Results sub-section displays:

- details of a model when pressing `Details` (only available for single selected item):
    - a small report on model performances,
    - the variables selected if` Feature Selection` has been activated,
    - the histogram of the importance of the variables (cf [http://topepo .github.io/caret/variable-importance.html](http://topepo.github.io/caret/variable-importance.html)), 
    - the prediction graphs depending on the observation.
- the comparison histogram of several models when pressing `Compare` (only available for multiple selected items):
    - the Root Mean Square Error (RMSE)
    - the coefficient of determination (Rsquared)
    - the Mean Absolute Error (MAE)
 

<img src="figures/Modeling_ResultsCompare.png" width="100%"/>

