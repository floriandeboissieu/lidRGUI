---
title: "Description de lidRGUI"
author: "Florian de Boissieu"
date: "`r Sys.Date()`"
output: 
  html_vignette: default
vignette: >
  %\VignetteIndexEntry{Description de lidRGUI}
  %\VignetteEngine{knitr::rmarkdown}
  \usepackage[utf8]{inputenc}
---

# Description de l'interface de lidRGUI
L'interface est lancée avec la commande:
```
library(lidRGUI)
lidRGUI()
```
Par défaut, le GUI sera lancé dans e Viewer de Rstudio. Tant que le programme est ouvert la session Rstudio courante n'est plus accessible. Pour stopper le GUI, soit appuyer sur le bouton Stop du Viewer, ou appuyer 2 fois sur la touche `Esc`.

Afin de garder la session Rstudio en cours opérationnelle pendant l'utilisation du GUI, l'interface peut être lancée en arrière plan (dans le browser par défault) en activant l'option `bg` (background):
```
lidRGUI(bg = TRUE)
```

L'interface se compose de 5 sections principales disponibles dans la bar de menu à gauche:

- Data: gestionnaire de données (import/export/sauvegarde de données multiformat) et cartographie.
- Transform: extraction et transformaion des données
- Metrics: calcul de métriques sur des données transformées
- Modeling: mise au point de modèles (limité au modèles de régression pour l'instant)
- Settings: paramètres génraux du l'interface, typiquement le nombre de coeurs pour le calcul.

De manière générale:

- un message d'information apparait lorsque la souris est maintenu au-dessus d'un bouton, d'un paramètre, d'un onglet.
- le bouton ![](figures/binfo.png) amène sur la page d'aide des méthodes utilisées, pour avoir des précisions sur les paramètres.
- le bouton `RUN` sous chaque module permet de lancer l'éxécution. Un message vert signale que tout s'est bien passé, un message rouge indique une erreur au cours du processus et que le résultat n'a pas pu être produit.


<img src="figures/GUI.png" width="100%"/>


## Data
La section Data comporte 2 boites rétractable:

* Import Data pour l'import de nouvelles données
* Data pour la liste des données et produit et la cartographie de ceux-ci

### Import Data
L'interface permet de charger 4 types de données:

- LAS Catalog: un dossier de LAS/LAZ composé de plusieurs dalles.
- Plots: un fichier de type csv comprenant les coordonnées et les dimensions des placettes, ainsi que les attributs mesurés sur le terrain typiquement.
- Raster: un fichier de type raster.
- RData: un fichier au format .RData (ou .rda) sauvegardé à partir de lidRGUI grâce au bouton ![](figures/bsave.png).

### Data
Cette sous section contient la liste des objets importés ou créés. Cette liste comprend les colonnes suivantes:

* __Actions__ qui sont respectivement:
    - historique ![](figures/bhistory.png) : affichage de l'historique,
    - export ![](figures/bexport.png) : export dans un fichier.
* __Name__: le nom assigné par lidRGUI,
* __Origin__: le traitement/l'action ayant permis de créer l'objet,
* __Method__: la méthode utilisée lorsqu'il y en a plusieurs (cf section Transform),
* __Path__: le chemin d'importation,
* __Class__: la classe de l'objet.

Au-dessus de la liste se trouve 3 boutons relatifs à la sélection d'objets:

- save ![](figures/bsave.png) : sauvegarder les objets dans un fichier .rda. Ce fichier sera rechargeable via l'import de .RData. Il peut être également chargé dans une session R avec la commande `load("nom_fu_fichier.rda")`, les données se trouvent alors dans la variable `data`. Il est donc conseillé d'utiliser ce bouton aussi souvent que possible.
- remove ![](figures/bremove.png) : retirer les objets de la liste.
- map ![](figures/bmap.png) : afficher les objets dans une carte interactive. Tous les fichiers ne sont pas visualisable sur une carte. Pour l'instant, il est possible d'afficher les objets de class LASCatalog, Plots, Raster, CHM, TreeTops. Les objets de type nuage de points (ExtractedLasPlots, DTMNormalized, ...) ainsi que les metriques, ne sont pas visualisable par ce moyen.

En-dessous de la liste s'affiche la carte interactive si elle est demandée.

## Transform
La section Transform est une succession d'outils/de traitement applicables aux nuages de points et aux produits dérivés. L'ordre de ces outils est généralement celui respecté pour la transforation du nuage de points:

- Plot Extraction: extraction les nuages de points à l'intérieur des placettes
- DTM Normalisation: normalisation du nuage de points au modèle numérique de terrain, i.e. Z ne représente plus l'altitude mais la hauteur par rapport au sol.
- Canopy Hieght Model: calcul du modèle de canopée, i.e. un raster par placette.
- Tree Top Detection: détection des sommets des arbres sur le modèle de canopée. La méthode de `treeExtraction (lidaRtRee)` permet également d'estimer la surface et le volume des couronnes.
- Tree Segmentation: assignation de chaque point du nuage de point à un arbre.

Les valeurs par défaut des paramètres sont généralement ceux définit dans la méthode ou ayant le mieux fonctionné dans les études précédentes.

Le module Plot Extraction permet de définir une zone buffer entourant le placette. L'ensemble de des traitements suivant s'appliquent à la plcette et son buffer afin d'éviter les effets de bord.


<img src="figures/TransformTab.png" width="100%"/>


## Metrics
La section Metrics est destinée au calcul de métriques. Pour l'instant seulement deux boites sont disponibles:

- Point Cloud Metrics: calcul de statistiques sur le nuage de points lui-même, i.e. sur la hauteur, l'intensité, la densité des échos, etc.
- Tree Tops Metrics: calcul de statisiques sur les hauteurs d'arbres, les volumes de couronnes. _Ces métriques sont calculable uniquement lorsque les TreeTops ont été extraits avec la méthode `treeExtraction (lidaRtRee)`_.

Chaque boite comprend en entrée l'objet sur lequel calculer les métriques. En-dessous se trouvent un double tableau avec:

- à gauche les jeux de métriques disponibles,
- à droite les métriques sélectionnées pour le calcul.
Contrairement à la section Transform, ici les jeux de métriques `stdmetrics` et `stdTreeMetrics` sont calculées sur la placette elle-même. Le jeu de métriques `ABAmodelMetrics` comprend une intersection en interne l'objet Plot définissant la postion et la taille des placettes.


<img src="figures/MetricsTab.png" width="100%"/>


## Modeling
La section Modeling comprend deux boites décrites ci-dessous:

- Model: définition et paramétrage du modèle.
- Results: affichage des résultats du modèle.

### Model
La sous-section Model permet de dfinir un modèle de type $y = f(X)$. Pour l'instant seul les modèles de régression sont possible. La définition du modèle se fit à travers les 3 onglets:

- Input: la définition des entrées, i.e. $X$. Comme pour les métriques, le double tableau permet de sélectionner les métriques de plusieurs jeux de métriques (voir illustration ci-dessous).
- Output: la définition de la sortie, i.e. $y$.
- Model: le type de modèles et les paramètres d'entrainement (Advanced tuning). Une préselection des algorithmes les plus usité est présenté dans le tableau. La case `Extend model list` permet d'étendre la liste à plus de 200 algorithmes (cf [https://topepo.github.io/caret/available-models.html](https://topepo.github.io/caret/available-models.html)). Seul les package correspondant à la pré-sélection sont inclus dans les dépendances, les autres packages doivent être installé manuellement. On notera également la case `Feature Selection` qui, lorsqu'elle est cochée, permet de faire une sélection de variable de type "Recursive Feature Elimination". La boite `Advanced tuning` permet de régler certains paramètres de pré-traitement (centrer, réduire, ...) ainsi que les paramètres d'évaluation du modèle.


<img src="figures/Modeling_InputTab.png" width="100%"/>


### Results
La sous-section Results permet de visualiser:

- les détail d'un modèle en appuyant sur `Details`, i.e. les détails du meilleur fit, les variables sélectionnés si `Feature Selection` a été activée, l'histogramme de l'importance des variables (cf [http://topepo.github.io/caret/variable-importance.html](http://topepo.github.io/caret/variable-importance.html)), les graph de prédiction en fonction de l'observation.
- l'histogramme de comparaison de plusieurs modèles en appuyant sur Compare:
 - la Root Mean Square Error (RMSE)
 - le coefficient de détermination (Rsquared)
 - la Mean Absolute Error (MAE)


<img src="figures/Modeling_ResultsCompare.png" width="100%"/>

