<# : chooser.bat
:: launches a File... Open sort of file chooser and outputs choice(s) to the console
:: https://stackoverflow.com/a/15885133/1683264

@echo off
cd %1
setlocal

for /f "delims=" %%I in ('powershell -noprofile -Sta "iex (${%~f0} | out-string)"') do (
    echo %%~I
)
goto :EOF

: end Batch portion / begin PowerShell hybrid chimera #>

Add-Type -AssemblyName System.Windows.Forms
$f = new-object System.Windows.Forms.FolderBrowserDialog
$f.Description = "Select a folder."
$f.SelectedPath = pwd
[void]$f.ShowDialog()
$f.SelectedPath
