# lidRGUI: Graphical User Interface for lidR (LIDAR processing package)
![licence](https://img.shields.io/badge/Licence-GPL--3-blue.svg)

**This project was a demonstrator: unfortunately it is not maintained anymore.**

![demo](doc/demo.gif)

Authors: Florian de Boissieu, David Lang

This package give a graphical user interface (GUI) to lidar-based data processing and modeling.

`lidRGUI` is based on the R packages `lidR`, `caret` and `leaflet`. It also integrates part of R Package `lidaRtRee`.

# Features
An overview of the graphical user interface is available in [english](doc/lidRGUI_en.md) and [french](doc/lidRGUI_fr.md).
Package `lidRGUI` gives graphical user interface to important features of:

* **lidR**
  * import Catalog of LAS tiles (as well as plots database)
  * extract LAS point clouds of plots
  * normalize point cloud to DTM
  * compute Canopy Height Model
  * detect tree tops
  * compute tree segmentation
  * compute plots-wide and tree-wide metrics

* **caret**
  * pre-processing of input features with correlation thresholds, center & scale, Yeo Johnson,
  * cross-validation parameters (type, data partitions, etc.),
  * `caret` regression & classification algorithms (>150),
  * feature selection loop,
  * standardize details, variable importance, observation-prediction interactive plots
  * model comparison summary
  
* **leaflet** to vizualize on map:
  * LAScatalog and plots location
  * rasters (such as DTM or plots CHM)
  * tree top location

All products can be saved in RData files for further analysis in R or RStudio, or exported individually in various file formats (LAS, tif, shapefile, csv, etc.).


# 1 Install

## 1.1 Dependencies
### 1.1.1 lidaRtRee
Package `lidaRtRee` is needed for the computation of some metrics.
```
devtools::install_git("https://gitlab.irstea.fr/jean-matthieu.monnet/lidaRtRee.git")
```

### 1.1.2 shinyjs
At the moment the version of package `shinyjs` available on CRAN has a bug on selector identification, submitted under [issue #164](https://github.com/daattali/shinyjs/issues/164).
The bug was fixed on github, thus install github version with R command:
```
devtools::install_github("daattali/shinyjs")
```

### 1.1.3 caret
There is a known bug in `caret` package when using pre-processing options "zv" and "corr". A patch is waiting to be accepted by caret maintainer. Until this fix is available on `CRAN` versions of `caret`, `caret` package should be installed with the following command lines:
```
devtools::install_github("topepo/recipes")
devtools::install_github("floriandeboissieu/caret/pkg/caret", ref = "patch-2")
```

### 1.1.4 Libraries (Linux only)
Dependencies (Ubuntu names):
- for devtools:
```
sudo apt-get install libcurl4-openssl-dev libssl-dev
```
- for lidRGUI package dependencies:
```
sudo add-apt-repository ppa:ubuntugis/ppa
sudo apt-get update
sudo apt-get install gdal-bin libgdal-dev 
sudo apt-get install libcairo2-dev libudunits2-dev libglu1-mesa-dev freeglut3-dev mesa-common-dev libv8-3.14-dev
```


## 1.2 Install package

In a R session:

1. install package `devtools` if not already done (it might install RTools in Windows)

```{r}
install.packages("devtools")
```

2. enter the following command line in an R session. *Warning: default options will automatically upgrade your R packages to the latest*, if not wanted set option `upgrade.denpendencies = FALSE` (see [devtools::install_git](https://cran.r-project.org/web/packages/devtools/devtools.pdf).

```
devtools::install_git("https://gitlab.irstea.fr/florian.deboissieu/lidRGUI.git")
```

## 1.3 Known errors at install
### 1.3.1 Error message in RStudio Console tab
Errors may occure during install or when launching `lidRGUI()` execution. It is usually due to missing packages or updates not automatically installed.

These errors can be solved installing or updating the packages manually, which can be done from Rstudio Package tab or with R function `install.packages`:
 - use source if asked. If it fails with source install binary first.
 - if after install the package does not appear in package list of RStudio tab `Packages`, restart R session from Rstudio menu `Session -> Restart R`

Here is a non-exaustive list of the resensed packages not installed although indirect dependencies:

* readr
* rvest
* zoo
* spacetime
* FNN
* recipes
* withr
* httpuv and dependencies (later)
* digest
* sp

### 1.3.2 `lidRGUI()` crash at start
If the command line `lidRGUI()` crashes, it can be due to missing package (refer to previous section) or to wrong `shinyjs` package (see section above).



### 1.3.3 Dalponte segmentation crash
There is a known bug in Dalponte segmentation method in `lidR` version 1.6.0. The bug was fixed in version 1.6.1.

# 2 Run GUI
Run command `lidRGUI()` from an Rstudio or R session. To run GUI in background and keep your on going session available, run command `lidRGUI(bg = TRUE)`.

