## TO DO
* Add buttons:
  * if possible `Edit` to edit properties, i.e. projection.
  * `viewer` buttons in data list table next to `export` buttons, and associated viewers to class
* Make user implementation possible, see shinyAce, example lasmetrics(lass, eval(parse(text = "mean(X)")).
* Add table explorer (partly done)
* Check writing extra bytes (each extra byte should be added)
* Add import capacity for metrics, treetops, laslist, etc. 
* Change data models to types of data
* extend exports to different types of files (ex: add shapefiles for data.frames, see TreeTopsData for example)
* Add voxelisation
* Add field id or a method getID in every models, to track/check id every time needed.

## lidRGUI v0.0.0.9012
### Bug fix
- global variables update on choose directory/file cancelling
- custom data table with opion paging instead of scroller
- fix bugs in caret for cohexistence of preProcess options "zv" and "corr"

### Change
- *lidR must be 1.6.1*, next releases will follow lidR evolution
- demo gif
- file and directory browsers on server with shinyFiles

### Add
- projection persitence at loading between LAScatalog and Plots
- classification models

## lidRGUI v0.0.0.9011
### Add
- Vignettes to package documentation

### Change
- **`gui()` becomes `lidRGUI()`**
- `save` button saves selected data in an object of the same name as the file basename without extension, example:
`data-2018-10-01` for file data-2018-10-01.rda.

### Bug fix
- Load/save long file path in Windows
- Busy indicators in load buttons
- Tree Metrics computes surface and volume for lidaRtRee tree tops

## lidRGUI v0.0.0.9010
### Bug fix
- Dalponte2016 segmentation method: needs 3 columns (X, Y, seed_id)
- any ID variable name for modeling
- write NA values in txt files

### change
- help buttons

## lidRGUI v0.0.0.9009
### Bug fix
- open and save dialogs for Windows
- Variable Importance graph : ggplotly replaced by ggplot

### change
- include tree tops detection of lidaRtRee in main tree tops methods and remove tree tops 2 section
- send settings at the end of sidebar
- remove workingDirectory in settings

## lidRGUI v0.0.0.9008
### Bug fix
- Dalponte bug workaround with CRAN version 1.6 of lidR
- Densities of tree top metrics
- save and export repeated access to modal dialog
- model data history
- train model with 100% of the data

### Add
- snapshots of figures in model results
- persistence of input and output directories

### Change
- Advanced tuning included in model tab
- save downloadHandler (not working in RStudio viewer) changed for local file explorer

## lidRGUI v0.0.0.9007
### Bug fix
- CSV plots loading "inputs" error
- Tree detection with Dalponte2016
- RF coputation and radial SVM details

## lidRGUI v0.0.0.9006
* Changes:
 * Plots loading: plots size has been moved from extraction to here for coherence and tree tops computation matters
 * leaflet: it is now in the listing section of DATA page and most of the layers (Catalog, Plots, CHM, Tree Tops) can be shown at the same time on the map. Layer control and reset view were added to leaflet display.

* Add buffer support at extraction. All transformations are computed with buffer to avoid edge effects. The point clouds and treetops in the buffer zone are filtered at metric computation.

* Add metrics of lidaRtRee: ABAmodelMetrics for point cloud and stdTreeMetrics for tree tops.


## lidRGUI v0.0.0.9005
* Add buttons:
  * `history` and `export` buttons in data list table to allow particular exports
  * `Delete` button (with bin icon) above data list
  * `Help` button with question mark icon next to preference, to access user documentation.
* Add reset map extent and layer choice buttons in leaflet
* Add version to models
* Add information display (e.g. with shinyTree) when clicking on datasets or with an `Info` button or adapted icon.
* Make scriptable functions of useful sections (e.g. leaflet, model figures) so it can be used in scripts.

* Fix bugs: see issues and commits

## lidRGUI v0.0.0.9004

### ENHANCEMENT
* Added example dataset
* Complex variable names of plots accepted, e.g. `G (m2/ha)`

### FIX
* Model details display for cases without performance standard deviation results.
* Remove empty las after filtering (for metric computation).


### CHANGES
* Modelisation tab: MODEL section becomes RESULTS section

## lidRGUI v0.0.0.9003

### ENHANCEMENT
* All available metrics of lidR
* Support of lidR 1.3.0+

### FIX
* No more error message on data loading cancel
